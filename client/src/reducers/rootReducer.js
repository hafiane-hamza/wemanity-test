import update from 'immutability-helper'

const DEFAULT_STATE = {
    department: 75,
    category: 'locations',
    minPrice: 100,
    maxPrice: 4000,
    minSurface: 0,
    maxSurface: 50,
    rooms: 1,
    lat: 48.8490713080018,
    lng: 2.32891601562504,
    zoom: 12,
    results: []
}

function rootReducer(state = DEFAULT_STATE, action) {
    switch (action.type) {
        case 'SET_DEPARTMENT':
            return update(state, {
                department: {
                    $set: action.department
                }
            })
        case 'SET_MIN_PRICE':
            return update(state, {
                minPrice: {
                    $set: action.minPrice
                }
            })
        case 'SET_MAX_PRICE':
            return update(state, {
                maxPrice: {
                    $set: action.maxPrice
                }
            })
        case 'SET_MIN_SURFACE':
            return update(state, {
                minSurface: {
                    $set: action.minSurface
                }
            })
        case 'SET_MAX_SURFACE':
            return update(state, {
                maxSurface: {
                    $set: action.maxSurface
                }
            })
        case 'SET_CATEGORY':
            return update(state, {
                category: {
                    $set: action.category
                }
            })
        case 'SET_ROOMS':
            return update(state, {
                rooms: {
                    $set: action.rooms
                }
            })
        case 'SET_RESULTS':
            return update(state, {
                results: {
                    $set: action.results
                }
            })
        case 'SET_CENTER':
            return update(state, {
                lat: {
                    $set: action.lat
                },
                lng: {
                    $set: action.lng
                },
                zoom: {
                    $set: 14
                }
            })
        default:
            return state
    }
}

export default rootReducer
