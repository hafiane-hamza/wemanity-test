import {
    takeLatest
} from 'redux-saga/effects'

import sendSearch from './sendSearch'

function* rootSaga() {
    yield takeLatest('SEND_SEARCH', sendSearch)
}

export default rootSaga
