import {
    call,
    put
} from 'redux-saga/effects'

import axios from 'axios'

const API_URL = process.env.REACT_APP_API_URL

function* sendSearch(action) {
    const params = {
        category: action.category,
        minPrice: action.minPrice,
        maxPrice: action.maxPrice,
        minSurface: action.minSurface,
        maxSurface: action.maxSurface,
        department: action.department,
        rooms: action.rooms
    }
    const res = yield call(
        [axios, 'get'],
        `${API_URL}/search`,
        {
            params
        }
    )

    yield put({
        type: 'SET_RESULTS',
        results: res.data
    })
}

export default sendSearch
