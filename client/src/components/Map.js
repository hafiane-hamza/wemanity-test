import React from 'react'

import {
    Map as LeafleftMap,
    TileLayer,
    Marker,
    Popup
} from 'react-leaflet'

import {
    connect
} from 'react-redux'

class Map extends React.Component {
    render() {
        const position = [this.props.lat, this.props.lng]
        return (
            <div className="appartment-map">
                <LeafleftMap center={position} zoom={this.props.zoom}>
                    <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {this.props.results.map(result => (
                        <Marker key={result.id}
                                position={[result.location.lat, result.location.lng]}>
                            <Popup>
                                {result.title}
                            </Popup>
                        </Marker>
                    ))}
                </LeafleftMap>

                {this.props.results.length > 0 ? (
                    <div className="results">
                        <div className="inner">
                            {this.props.results.map(result => (
                                <div className="result">
                                    <h2 className="title">{result.title}</h2>
                                    {(typeof result.images === 'object' && result.images.length > 0) ? (
                                        <img src={result.images[0]}
                                             alt={result.title}
                                             className="thumbnail" />
                                    ) : null}

                                    <p>
                                        Prix: {result.price}€
                                    </p>

                                    <a href="/"
                                       onClick={e => {
                                           e.preventDefault()
                                           this.props.dispatch({
                                               type: 'SET_CENTER',
                                               lat: result.location.lat,
                                               lng: result.location.lng
                                           })
                                       }}
                                       className="center-button">
                                        Centrer
                                    </a>
                                </div>
                            ))}
                        </div>
                    </div>
                ) : null}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    results: state.results,
    lat: state.lat,
    lng: state.lng,
    zoom: state.zoom
})

export default connect(mapStateToProps)(Map)
