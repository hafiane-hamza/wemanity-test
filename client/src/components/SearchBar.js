import React from 'react'

import {
    connect
} from 'react-redux'

class SearchBar extends React.Component {

    render() {
        return (
            <div className="appartment-searchbar">
                <h1 className="title">Recherche</h1>

                <h2>Filtres</h2>

                <div>
                    <label>Département</label>
                    <select name="department"
                            value={this.props.department}
                            onChange={(e) => {
                                this.props.dispatch({
                                    type: 'SET_DEPARTMENT',
                                    department: e.target.value
                                })
                            }}>
                        <option value="36">36</option>
                        <option value="37">37</option>
                        <option value="41">41</option>
                        <option value="49">49</option>
                        <option value="72">72</option>
                        <option value="75">75</option>
                        <option value="79">79</option>
                        <option value="86">86</option>
                    </select>
                </div>

                <div>
                    <label>Catégorie</label>
                    <select name="category"
                            value={this.props.category}
                            onChange={(e) => {
                                this.props.dispatch({
                                    type: 'SET_CATEGORY',
                                    category: e.target.value
                                })
                            }}>
                        <option value="ventes_immobilieres">Ventes Immobilières</option>
                        <option value="locations">Locations </option>
                    </select>
                </div>

                <div>
                    <label>Prix Minimum</label>
                    <input type="text"
                           value={this.props.minPrice}
                           onChange={(e) => {
                               this.props.dispatch({
                                   type: 'SET_MIN_PRICE',
                                   minPrice: e.target.value
                               })
                           }}></input> €
                </div>

                <div>
                    <label>Prix Maximum</label>
                    <input type="text"
                           value={this.props.maxPrice}
                           onChange={(e) => {
                               this.props.dispatch({
                                   type: 'SET_MAX_PRICE',
                                   maxPrice: e.target.value
                               })
                           }}></input> €
                </div>

                <div>
                    <label>Surface Minimum</label>
                    <input type="text"
                           value={this.props.minSurface}
                           onChange={(e) => {
                               this.props.dispatch({
                                   type: 'SET_MIN_SURFACE',
                                   minSurface: e.target.value
                               })
                           }}></input> m²
                </div>

                <div>
                    <label>Surface Maximum</label>
                    <input type="text"
                           value={this.props.maxSurface}
                           onChange={(e) => {
                               this.props.dispatch({
                                   type: 'SET_MAX_SURFACE',
                                   maxSurface: e.target.value
                               })
                           }}></input> m²
                </div>

                <div>
                    <label>Pièces</label>
                    <select name="pieces"
                            value={this.props.rooms}
                            onChange={(e) => {
                                this.props.dispatch({
                                    type: 'SET_ROOMS',
                                    rooms: e.target.value
                                })
                            }}>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>

                <button onClick={this.search.bind(this)}>Recherche</button>
            </div>
        )
    }

    search(e) {
        e.preventDefault()
        this.props.dispatch({
            type: 'SEND_SEARCH',
            department: this.props.department,
            category: this.props.category,
            minPrice: this.props.minPrice,
            maxPrice: this.props.maxPrice,
            minSurface: this.props.minSurface,
            maxSurface: this.props.maxSurface,
            rooms: this.props.rooms
        })
    }
}

const mapStateToProps = state => ({
    department: state.department,
    category: state.category,
    minPrice: state.minPrice,
    maxPrice: state.maxPrice,
    minSurface: state.minSurface,
    maxSurface: state.maxSurface,
    rooms: state.rooms,
})

export default connect(mapStateToProps)(SearchBar)
