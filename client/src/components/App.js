import React from 'react'

import Map from './Map.js'
import SearchBar from './SearchBar.js'

function App() {
    return (
        <div>
            <Map />
            <SearchBar />
        </div>
    );
}

export default App;
