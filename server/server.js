const PORT = 4040
const ADDRESS = "127.0.0.1"

const express = require('express')
const leboncoin = require('leboncoin-api')
const cors = require('cors')

const app = express()

app.use(cors())

app.get('/search', (req, res) => {
    const search = new leboncoin.Search()

    if (typeof req.query.category === 'string' &&
        ['locations', 'ventes_immobilieres'].indexOf(req.query.category) > -1) {
        search.setCategory(req.query.category)
    }

    try {
        let price = {}
        if (typeof req.query.minPrice === 'string') {
            price.min = parseFloat(req.query.minPrice)
        }
        if (typeof req.query.maxPrice === 'string') {
            price.max = parseFloat(req.query.maxPrice)
        }
        if (typeof price.min !== 'undefined' ||
            typeof price.max !== 'undefined') {
            search.addSearchExtra('price', price)
        }
    }
    catch {
        return res.json([])
    }

    try {
        let square = {}
        if (typeof req.query.minSurface === 'string') {
            square.min = parseFloat(req.query.minSurface)
        }
        if (typeof req.query.maxSurface === 'string') {
            square.max = parseFloat(req.query.maxSurface)
        }
        if (typeof square.min !== 'undefined' ||
            typeof square.max !== 'undefined') {
            search.addSearchExtra('square', square)
        }
    }
    catch {
        return res.json([])
    }

    try {
        if (typeof req.query.rooms === 'string') {
            let rooms = parseInt(req.query.rooms)
            search.addSearchExtra('rooms', rooms)
        }
    }
    catch {
        return res.json([])
    }

    if (typeof req.query.department === 'string') {
        search.setDepartment(req.query.department)
    }

    search.setPage(1)
    search.run().then((data) => {
        console.log('1', data)
        console.log('2', data.results)
        res.json(data.results)
    })
})

app.listen(PORT, ADDRESS, () => {
    console.log(`server listening at http://${ADDRESS}:${PORT}`)
})
